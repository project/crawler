CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

 INTRODUCTION
 ------------

 Crawler module provides list links of any specified url. Links Stored in Custom Entity called Crawl Job.

 REQUIREMENTS
 ------------

 This module requires spatie/crawler(https://github.com/spatie/crawler), which will be run on drupal 8.8.

 INSTALLATION
 ------------
  - Install the Crawler module as you would normally install a contributed Drupal
    module.

 Configuration
 -------------
  - After installing this module , navigate to "/admin/content/crawl-job", click on the "Add Crawl Job", you will be redirected to Crawl Job Entity Form.
  - Provide the URL to crawl and then click on Save, On clicking on Save, Batch Process will start which will crawl the all url availble on the provided url, and take the user back to the crawl job listing page.
