<?php

namespace Drupal\crawler\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the crawl job entity edit forms.
 */
class CrawlJobForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    // Fetch and Save the results of Url grabber.
    $source_url = $form_state->getValue('source')[0]['uri'];
    $batch = [
      'title' => $this->t('Crawling the Links from the Source @source', ['@source' => $source_url]),
      'operations' => [
        [
          '\Drupal\crawler\CrawlBatch::getLinks',
          [$entity->id(), $source_url],
        ],
      ],
      'finished' => '\Drupal\crawler\CrawlBatch::getLinksFinishedCallback',
    ];
    batch_set($batch);

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New crawl job %label has been created.', $message_arguments));
      $this->logger('crawler')->notice('Created new crawl job %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The crawl job %label has been updated.', $message_arguments));
      $this->logger('crawler')->notice('Updated crawl job %label.', $logger_arguments);
    }
  }

}
