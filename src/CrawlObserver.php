<?php

namespace Drupal\crawler;

use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Spatie\Crawler\CrawlObserver as SpatieCrawlObserver;

/**
 * Class CrawlerObserver.
 *
 * @package Drupal\crawler
 */
class CrawlObserver extends SpatieCrawlObserver {
  /**
   * URL'S property to store all the url's fetched from Crawler.
   *
   * @var array
   */
  protected $urls = [];

  /**
   * Property to store all the success url's fetched from Spatie Crawler.
   *
   * @var array
   */
  protected $urlsSuccess = [];

  /**
   * Property to store all the failed url's fetched from Spatie Crawler.
   *
   * @var array
   */
  protected $urlsFailed = [];

  /**
   * {@inheritdoc}
   */
  public function crawled(
    UriInterface $url,
    ResponseInterface $response,
    UriInterface $foundOnUrl = NULL
  ) {
    $this->urlsSuccess[] = $url->__toString();
  }

  /**
   * {@inheritdoc}
   */
  public function crawlFailed(UriInterface $url, RequestException $requestException, UriInterface $foundOnUrl = NULL) {
    $this->urlsFailed[] = $url->__toString();
  }

  /**
   * {@inheritdoc}
   */
  public function willCrawl(UriInterface $url) {
    $this->urls[] = $url;
  }

  /**
   * Get ALL URl's.
   *
   * @return array|strings
   *   URL's List.
   */
  public function getUrls() {
    return $this->urls;
  }

  /**
   * Get All Crawled Success URL's.
   *
   * @return array|strings
   *   URL's List.
   */
  public function getSuccessUrls() {
    return $this->urlsSuccess;
  }

  /**
   * Get All Failed Crawled Url's.
   *
   * @return array|strings
   *   URL's List.
   */
  public function getFailedUrls() {
    return $this->urlsFailed;
  }

}
