<?php

namespace Drupal\crawler\services;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use GuzzleHttp\Client;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\RequestOptions;
use Spatie\Crawler\Crawler;
use Spatie\Crawler\CrawlInternalUrls;
use Drupal\crawler\CrawlObserver;
use Drupal\crawler\CrawlProfile;

/**
 * CrawlManager Class.
 */
class CrawlManager {

  protected $httpClient;

  private $links = [];

  private $iDeep;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * The serialization class to use.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $serializer;

  /**
   * Constructs the ProductAddToCartForm.
   */
  public function __construct(Client $httpClient, LoggerChannelFactoryInterface $logger, EntityTypeManagerInterface $entity_manager, SerializationInterface $serializer) {
    $this->httpClient = $httpClient;
    $this->logger = $logger;
    $this->iDeep = 0;
    $this->entityMgr = $entity_manager;
    $this->serializer = $serializer;
  }

  /**
   * Get all links of a url.
   */
  public function getLinks($url) {
    $observer = new CrawlObserver();
    $options = [
        RequestOptions::COOKIES => true,
        RequestOptions::CONNECT_TIMEOUT => 10,
        RequestOptions::TIMEOUT => 10,
        RequestOptions::ALLOW_REDIRECTS => false,
        RequestOptions::HEADERS => [
            'User-Agent' => '*',
        ],
        'verify' => false,
    ];
    Crawler::create($options)
      ->setCrawlObserver($observer)
      ->setCrawlProfile(new CrawlProfile($url))
      ->startCrawling($url);
    $failed_urls = $observer->getFailedUrls();
    if (!empty($failed_urls)) {
      $this->logger->get('crawler')->error(t('There was an %error while crawling the %url', ['%error' => serialize($observer->getFailedUrls()), '%url' => $url]));
    }
    $success_urls = $observer->getSuccessUrls();
    return !empty($success_urls) ? $success_urls : [];
  }

  /**
   * Callback to get url patterns.
   *
   * @param array $urls
   *   URL'S list.
   *
   * @return array
   *   Array contains links sorted based on patterns.
   */
  public function getUrlsPatterns(array $urls) {
    $patterns = [];
    foreach ($urls as $value) {
      $parse_url = parse_url($value);
      if (isset($parse_url['path'])) {
        $data = array_filter(explode('/', $parse_url['path']));
        if (isset($data[1])) {
          $ext = substr(strrchr($data[1], '.'), 1);
          if (in_array($ext, ['php', 'asp', 'aspx', 'html'])) {
            $patterns[] = isset($data[2]) ? '/' . $data[2] . '/' : '';
          }
          else {
            $patterns[] = isset($data[1]) ? '/' . $data[1] . '/' : '';
          }
        }
      }
    }
    return array_filter(array_unique($patterns));
  }

  /**
   * Provides URL's based on pattern.
   *
   * @param string $pattern
   *   URL Pattern.
   * @param int $crawl_job_id
   *   CrawlJOb entity ID.
   *
   * @return string|array
   *   List of links based on pattern     *
   */
  public function getUrlByPattern($pattern, $crawl_job_id) {
    $crawl_job = $this->entityMgr->getStorage('crawl_job')->load($crawl_job_id);
    $mapped_links = $this->serializer->decode($crawl_job->field_patterns_links->value);
    if (!empty($mapped_links)) {
      $source_url = $crawl_job->get('source')->getValue()[0]['uri'];
      return isset($mapped_links[$source_url]['urls'][$pattern]) ? $mapped_links[$source_url]['urls'][$pattern] : [];
    }
    return '';
  }

  /**
   * List All Crawled URL's.
   *
   * @param string $source_url
   *   Source URL.
   * @param array $urls
   *   Crawled URL'S List.
   * @param array $patterns
   *   Links Patterns.
   *
   * @return array
   *   Matched Links.
   */
  public function getAllPatternUrls($source_url, array $urls, array $patterns) {
    $matched_links = [];
    foreach ($urls as $value) {
      foreach ($patterns as $v) {
        if (strpos($value, $v) == TRUE) {
          $matched_links[$source_url]['urls'][$v][] = $value;
        }
      }
    }
    return $matched_links;
  }

}
