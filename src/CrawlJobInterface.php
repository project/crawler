<?php

namespace Drupal\crawler;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a crawl job entity type.
 */
interface CrawlJobInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the crawl job title.
   *
   * @return string
   *   Title of the crawl job.
   */
  public function getTitle();

  /**
   * Sets the crawl job title.
   *
   * @param string $title
   *   The crawl job title.
   *
   * @return \Drupal\crawler\CrawlJobInterface
   *   The called crawl job entity.
   */
  public function setTitle($title);

  /**
   * Gets the crawl job creation timestamp.
   *
   * @return int
   *   Creation timestamp of the crawl job.
   */
  public function getCreatedTime();

  /**
   * Sets the crawl job creation timestamp.
   *
   * @param int $timestamp
   *   The crawl job creation timestamp.
   *
   * @return \Drupal\crawler\CrawlJobInterface
   *   The called crawl job entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the crawl job status.
   *
   * @return bool
   *   TRUE if the crawl job is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the crawl job status.
   *
   * @param bool $status
   *   TRUE to enable this crawl job, FALSE to disable.
   *
   * @return \Drupal\crawler\CrawlJobInterface
   *   The called crawl job entity.
   */
  public function setStatus($status);

}
