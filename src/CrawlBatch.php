<?php

namespace Drupal\crawler;

/**
 * CrawlBatch class.
 */
class CrawlBatch {

  /**
   * GetLinks Callback.
   */
  public static function getLinks($entity_id, $source_url, &$context) {
    $message = 'Grabbing the Links from the Source...';
    $results = \Drupal::service('crawler.manager')->getLinks($source_url);
    $context['message'] = $message;
    $context['results'] = $results;
    $crawl_job = entity_load('crawl_job', $entity_id);
    $serializer = \Drupal::service('serialization.phpserialize');
    $crawl_job->results = $serializer->encode($results);
    $patterns = \Drupal::service('crawler.manager')->getUrlsPatterns($results);
    $patterns_urls = \Drupal::service('crawler.manager')->getAllPatternUrls($source_url, $results, $patterns);
    $crawl_job->patterns = $serializer->encode($patterns);
    $crawl_job->patterns_urls = $serializer->encode($patterns_urls);
    $crawl_job->save();
  }

  /**
   * Batch Process Finished callback.
   */
  public static function getLinksFinishedCallback($success, $results, $operations) {
    if ($success) {
      $message = t('Finished grabbing the links from the source.');
    }
    else {
      $message = t('Finished with an error.');
    }
    drupal_set_message($message);
  }

}
