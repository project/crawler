<?php

namespace Drupal\crawler;

use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\UriInterface;
use Spatie\Crawler\CrawlProfile as SpatieCrawlProfile;

/**
 * Class CrawlProfile.
 *
 * @package Drupal\crawler
 */
class CrawlProfile extends SpatieCrawlProfile {

    /**
     * @var Uri|UriInterface
     */
    protected $baseUrl;

    /**
     * CrawlProfile constructor.
     * 
     * @param $baseUrl
     */
    public function __construct($baseUrl)
    {
        if (! $baseUrl instanceof UriInterface) {
            $baseUrl = new Uri($baseUrl);
        }

        $this->baseUrl = $baseUrl;
    }

    /**
     * {@inheritDoc}
     */
    public function shouldCrawl(UriInterface $url): bool
    {
        if(preg_match('(.jpg|.png|.tiff|.gif|.txt|.zip|.doc|.docx|.odt|.xls|.ppt|.jpeg|.js|.css)', $url->getPath()) === 1) {
            return FALSE;
        }
        else if($this->baseUrl->getHost() === $url->getHost()){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
}
